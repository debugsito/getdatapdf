const PDFExtract = require('pdf.js-extract').PDFExtract;
const pdfExtract = new PDFExtract();
const options = {
    firstPage: 1, // default:`1` - start extract at page nr
    lastPage: 2 //
}; /* see below */

const Sequelize = require('sequelize');

const sequelize = new Sequelize('datos_pdf', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    logging: false,
    dialectOptions: {
     connectTimeout: 60000
    }
});

const Dato = sequelize.define('datos', {
    paciente: Sequelize.TEXT,
    paciente_edad: Sequelize.TEXT,
    conyugue: Sequelize.TEXT,
    conyugue_edad: Sequelize.TEXT,
    medico: Sequelize.TEXT,
    fecha: Sequelize.TEXT,
    metodo_obtencion: Sequelize.TEXT,
    dias_abstinencia: Sequelize.TEXT,
    hora_emision: Sequelize.TEXT,
    hora_analisis: Sequelize.TEXT,
    volumen: Sequelize.DOUBLE,
    viscosidad: Sequelize.TEXT,
    ph: Sequelize.DOUBLE,
    aspecto: Sequelize.TEXT,
    licuefaccion: Sequelize.TEXT,
    color: Sequelize.TEXT,
    nro_espermatozoides_por_ml: Sequelize.DOUBLE,
    nro_total_espermatozoides: Sequelize.DOUBLE,
    celulas_redondas: Sequelize.DOUBLE,
    leucocitos: Sequelize.DOUBLE,
    espermatozoides_viables_porcentaje: Sequelize.DOUBLE,
    movilidad_progresiva: Sequelize.DOUBLE,
    movilidad_no_progresiva: Sequelize.DOUBLE,
    inmoviles: Sequelize.DOUBLE,
    tot_mov_progresivos: Sequelize.DOUBLE,
    normales: Sequelize.DOUBLE,
    anormales: Sequelize.DOUBLE,
    cabeza: Sequelize.DOUBLE,
    pieza_intermedia: Sequelize.DOUBLE,
    cola: Sequelize.DOUBLE,
    inmaduros: Sequelize.DOUBLE,
    indice_teratozoospermia: Sequelize.DOUBLE,
});

//requiring path and fs modules
const path = require('path');
const fs = require('fs');
//joining path of directory
const directoryPath = path.join(__dirname, 'files');
//passsing directoryPath and callback function

let pacientes = [];
fs.readdir(directoryPath, function (err, files) {
    //handling error
    if (err) {
        return console.log('Unable to scan directory: ' + err);
    }
    //listing all files using forEach
    files.forEach(function (file) {
        // Do whatever you want to do with the file
        pdfExtract.extract('files/'+file, options, async (err, data) => {
            if (err) return console.log(err);
            let paciente = {};
            let parte = '';
            let count_parte = 1;
            //console.log(data.pages);
            data.pages[0].content.forEach(item =>{
                // console.log(item);
                // if(item.x === 161.25 && item.y === 206.04999999999995){
                //     console.log("paciente: ", item.str);
                //     paciente.paciente = item.str;
                // }
                parte = parte + item.str;

                if(item.str.indexOf(":") >=0 && item.str.indexOf(":") !==2){
                    //console.log("==============",count_parte);
                    //console.log("+++++++++",parte);
                    if(count_parte===2){
                        paciente.paciente = parte.split(" ").slice(0,-3).join(" ").trim();
                    }
                    if(count_parte===3){
                        paciente.paciente_edad = parte.split(" ").slice(0,-4).join(" ").trim();
                    }

                    if(count_parte===4){
                        paciente.conyugue = parte.split(" ").slice(0,-3).join(" ").trim();
                    }

                    if(count_parte===5){
                        paciente.conyugue_edad = parte.split(" ").slice(0,-3).join(" ").trim();
                    }

                    if(count_parte===6){
                        paciente.medico = parte.split(" ").slice(0,-3).join(" ").trim();
                    }

                    if(count_parte===7){
                        paciente.fecha = parte.split(" ").slice(0,-7).join(" ").trim();
                        let hoy = new Date(paciente.fecha);
                        paciente.fecha = `${hoy.getFullYear()}` + "-" + `${hoy.getMonth() + 1}`.padStart(2, "0") + "-" + `${hoy.getDate()}`.padStart(2, "0");
                        paciente.metodo_obtencion = parte.split(":").slice(1).join("").trim();
                    }

                    if(count_parte===8){
                        paciente.metodo_obtencion = parte.split(" ").slice(0,-5).join(" ").trim()?parte.split(" ").slice(0,-5).join(" ").trim():paciente.metodo_obtencion;
                    }

                    if(count_parte===9){
                        paciente.dias_abstinencia = parte.split(" ").slice(0,-5).join(" ").trim();
                        paciente.hora_emision = item.str.substr(2);
                    }

                    if(count_parte===10){
                        let hora = parte.split(" ").slice(0,-5).join(" ").trim();
                        if(hora.length<=3){
                            paciente.hora_emision = paciente.hora_emision +" " + parte.split(" ").slice(0,-5).join(" ").trim();
                        }else{
                            paciente.hora_emision = parte.split(" ").slice(0,-5).join(" ").trim();
                        }
                    }

                    if(count_parte===11){
                        paciente.hora_analisis = parte.split(" ").slice(0,-6).join(" ").trim();
                    }

                    if(count_parte===12){
                        paciente.volumen = parte.split(" ").slice(0,-12).join(" ").trim();
                    }

                    if(count_parte===13){
                        paciente.viscosidad = parte.split(" ").slice(0,-4).join(" ").trim();
                    }

                    if(count_parte===14){
                        paciente.ph = parte.split(" ").slice(0,-10).join(" ").trim();
                    }

                    if(count_parte===15){
                        paciente.aspecto = parte.split(" ").slice(0,-3).join(" ").trim();
                    }

                    if(count_parte===16){
                        paciente.licuefaccion = parte.split(" ").slice(0,-3).join(" ").trim();
                    }

                    if(count_parte===17){
                        paciente.color = parte.split(" ").slice(0,-13).join(" ").trim();
                    }

                    if(count_parte===18){
                        paciente.nro_espermatozoides_por_ml = parte.split(" ").slice(0,-17).join(" ").trim();
                    }

                    if(count_parte===19){
                        paciente.nro_total_espermatozoides = parte.split(" ").slice(0,-15).join(" ").trim();
                    }

                    if(count_parte===20){
                        paciente.celulas_redondas = parte.split(" ").slice(0,-12).join(" ").trim();
                    }

                    if(count_parte===21){
                        paciente.leucocitos = parte.split(" ").slice(0,-22).join(" ").trim();
                    }

                    parte = '';
                    ++count_parte;

                }
            });

            paciente.espermatozoides_viables_porcentaje = parte.split(" ").slice(0,-15).join(" ").trim();

            parte = '';
            count_parte = 1;
            //segunda hoja
            data.pages[1].content.forEach(item =>{
                // console.log(item);
                // if(item.x === 161.25 && item.y === 206.04999999999995){
                //     console.log("paciente: ", item.str);
                //     paciente.paciente = item.str;
                // }
                parte = parte + item.str;

                if(item.str.indexOf(":") >=0 && item.str.indexOf(":") !==2){
                     console.log("==============",count_parte);
                     console.log("+++++++++",parte);
                    if(count_parte===2){
                        paciente.movilidad_progresiva = parte.split(" ").slice(0,-12).join(" ").trim();
                    }
                    if(count_parte===3){
                        paciente.movilidad_no_progresiva = parte.split(" ").slice(0,-13).join(" ").trim();
                    }

                    if(count_parte===4){
                        paciente.inmoviles = parte.split(" ").slice(0,-31).join(" ").trim();
                    }

                    if(count_parte===5){
                        paciente.tot_mov_progresivos = parte.split(" ").slice(0,-12).join(" ").trim();
                    }

                    if(count_parte===6){
                        paciente.normales = parte.split(" ").slice(0,-8).join(" ").trim();
                     }

                    if(count_parte===7){
                        paciente.anormales = parte.split(" ").slice(0,-15).join(" ").trim();
                    }

                    if(count_parte===8){
                        paciente.cabeza = parte.split(" ").slice(0,-12).join(" ").trim()?parte.split(" ").slice(0,-5).join(" ").trim():paciente.metodo_obtencion;
                    }

                    if(count_parte===9){
                        paciente.pieza_intermedia = parte.split(" ").slice(0,-12).join(" ").trim();
                    }

                    if(count_parte===10){
                        paciente.cola = parte.split(" ").slice(0,-24).join(" ").trim();
                    }

                    if(count_parte===11){
                        paciente.inmaduros = parte.split(" ").slice(0,-6).join(" ").trim();
                    }

                    if(count_parte===12){
                        paciente.indice_teratozoospermia = parte.split(" ").slice(0,-1).join(" ").trim();
                    }

                    if(count_parte===13){
                        paciente.observaciones = parte.split(" ").slice(0,-1).join(" ").trim();
                    }

                    parte = '';
                    ++count_parte;

                }
            });
        
            console.log(paciente);
            await Dato.create(paciente);
            pacientes.push(paciente);

        });
    });
});

